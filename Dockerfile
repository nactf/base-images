ARG base
FROM ${base}

ADD entry.sh /

ENTRYPOINT ["/entry.sh"]
